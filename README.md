# my_code

## Project setup（下载依赖）
```
npm install
```

### Compiles and hot-reloads for development（本地运行）
```
npm run serve
```

### Compiles and minifies for production（生产打包）
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### 项目文件大纲

![](D:\code\reference\my_code\my_code.jpg)

```
assets --- 里面包括了css样式和img图片
components --空（用于保存公共方法）
router -- 里面包括了路由的基础设置
utils -- 里面是菜单数据和token的相关方法
views -- 里面相关的html页面
```

<img src="D:\code\reference\my_code\my.jpg" style="zoom: 67%;" />

```
dist -- 生产包
node_modules --依赖包
public --地址栏图标
src --html加相关设置
.gitignore --git自动生成的配置文件(不用动)
babel.config.js -- 自动生成配置文件
jsconfig.json --自动生成配置文件
package.json  --自动生成配置文件(相关的下载包比如element)
package-lock.json --自动生成配置文件
README.md -- 系统说明

```

