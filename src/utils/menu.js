export var ment = [
    {
        name: '系统首页', icon: 'el-icon-s-home',
        children: [
            { name: '系统首页', path: '/home/index' },
        ]
    },
    {
        name: '系统消息', icon: 'el-icon-s-promotion',
        children: [
            { name: '我的消息', path: '/home/Meinformation' },
            { name: '系统公告', path: '/home/Announcement' }
        ]
    },
    {
        name: '处理中心', icon: 'el-icon-s-claim', path: '3',
        children: [
            { name: '用户', path: '/home/user' },
            { name: '产品', path: '/home/productControl' }
        ]
    },
    {
        name: '个人中心', icon: 'el-icon-s-tools', path: '6',
        children: [
            { name: '密码修改', path: '/home/password' },
            { name: '名称修改', path: '/home/userName' }
        ]
    }
]