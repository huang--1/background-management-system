// 封装token方法

// 设置键值
var HM_PC = "HM_PC"

// 保存token
export function setItem(token) {
    sessionStorage.setItem(HM_PC, JSON.stringify(token))
}
// 获取token
export function getItem() {
    // 使用try 进行错误兼容，防止出错
    try {
        return JSON.parse(sessionStorage.getItem(HM_PC))
    } catch (err) {
        console.log(err)
    }
}
// 删除token 
export function removeItem() {
    sessionStorage.removeItem(HM_PC)
}