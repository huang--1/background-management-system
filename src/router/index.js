import vue from 'vue'
import VueRouter from 'vue-router'
import { getItem } from '@/utils/store'
// 使用路由
vue.use(VueRouter)
const router = new VueRouter({
    mode: 'history',
    routes: [
        { path: '/', redirect: '/home/index' },
        { path: '/logi', name: 'logi', component: () => import('../views/logi/logi.vue') },
        {
            path: '/home', name: '/home', component: () => import('../views/home/home.vue'),
            meta: { title: '首页' },
            children: [
                {
                    path: 'index', name: '/index', component: () => import('../views/home/index.vue'),
                    meta: { title: '首页' },
                },
                {
                    path: 'Meinformation', name: '/Meinformation', component: () => import('../views/information/Meinformation.vue'),
                    meta: { title: '我的消息' },
                },
                {
                    path: 'Announcement', name: '/Announcement', component: () => import('../views/information/Announcement.vue'),
                    meta: { title: '系统公告' },
                },
                {
                    path: 'user', name: '/user', component: () => import('../views/dispose/user.vue'),
                    meta: { title: '用户' },
                },
                {
                    path: 'productControl', name: '/productControl', component: () => import('../views/dispose/productControl.vue'),
                    meta: { title: '产品' },
                },
                {
                    path: 'password', name: '/password', component: () => import('../views/personalCenter/password.vue'),
                    meta: { title: '密码修改' },
                },
                {
                    path: 'userName', name: '/userName', component: () => import('../views/personalCenter/userName.vue'),
                    meta: { title: '名称修改' },
                },
            ]
        },

    ]
})
// 路由守卫设置
router.beforeEach((to, from, next) => {
    const token = getItem()
    // 如果他没有token并且去的地方不是登录页,让他跳掉到登录页
    if (!token && to.path !== '/logi') {
        return next('/logi')
    } else if (token && to.path === '/logi') {
        // 如果他有token并且去的地方是登录页,让他跳掉到首页
        return next('/home/index')
    }
    next()
})
// 重复点击导航，控制台路由报错
const VueRouterPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(to) {
    return VueRouterPush.call(this, to).catch(err => err)
}

export default router